import h5py
import sys
import json
import numpy as np

if len(sys.argv)<0:
    print "Usage: python genbrainvizfile.py <brainfile1>.h5"
    print "Note: To have several level of details of the same brain : python genbrainvizfile.py <brainfile1>.h5 <brainfile2>.h5 ..."
    exit()

n = len(sys.argv)-1
i = 1

jsonDict = {}
currentDict = jsonDict

if (n>1):
    jsonDict['brain_lod'] = []

for i in range(n):

    if (n>1):
        currentDict = {}
        jsonDict['brain_lod'].append(currentDict)

    f = h5py.File(sys.argv[i+1],'r')

    if 'x' not in f.keys() or 'y' not in f.keys() or 'z' not in f.keys():
        print "no xyz coordinates available"
        exit()

    print "processing coordinates..."

    currentDict['positions'] = []

    for i in range(len(f['x'])):
        if i>0 and ((i%10000)==0):
                print str(i) + "/" + str(len(f['x']))
        
        xyz = [np.float64(f['x'][i]),np.float64(f['y'][i]),np.float64(f['z'][i])]
        currentDict['positions'].append(xyz)

    if 'colorx' in f.keys() and 'colory' in f.keys() and 'colorz' in f.keys():
        print "processing colors..."
        currentDict['colors'] = []
        for i in range(len(f['colorx'])):
            if i>0 and ((i%10000)==0):
                print str(i) + "/" + str(len(f['colorx']))

            color = [np.float64(f['colorx'][i]),np.float64(f['colory'][i]),np.float64(f['colorz'][i])]
            currentDict['colors'].append(color)

outName = sys.argv[1].split('.')[0]+".json"

print "export json file: "+outName

with open(outName, 'w') as outfile:
    json.dump(jsonDict, outfile)
    print "done!"


